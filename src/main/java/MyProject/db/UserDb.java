package MyProject.db;

import MyProject.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDb implements db<User> {
    SqlConnection sqlConnection;

    public UserDb() {
        super();
        sqlConnection = new SqlConnection();
    }

    @Override
    public boolean add(User user) {
        try {
            PreparedStatement preparedStatement = sqlConnection.preparedStatement("Insert into users(username,gender, " +
                    "password," +
                    "email," +
                   "photo, position) values (?,?,?,?,?,?)");
            System.out.println("username + "+ user.getFullName());
            preparedStatement.setString(1, user.getFullName());
            System.out.println("gender + "+ user.getGender());
            preparedStatement.setString(2, user.getGender());
            System.out.println("password + "+ user.getPassword());
            preparedStatement.setString(3, user.getPassword());
            System.out.println("password + "+ user.getEmail());
            preparedStatement.setString(4, user.getEmail());

            preparedStatement.setString(5, user.getUrlPhoto());
            System.out.println("position + "+ user.getPosition());
            preparedStatement.setString(6, user.getPosition());
            System.out.println("Insert process successfully ");
            System.out.println("in userdb " + user.toString());
           return preparedStatement.execute();

        } catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Optional<User> getOneByEmail(String email, String password) {
        try {
            PreparedStatement preparedStatement = sqlConnection.preparedStatement("Select * from users where email =?");
              preparedStatement.setString(1, email);

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                if(resultSet.getString("password").equals(password)){
                    System.out.println("trueeeee");
                User user = new User(resultSet.getInt("id"),
                        resultSet.getString("username"),
                        resultSet.getString("gender"),
                        resultSet.getString("password"),
                        resultSet.getString("email"),
                        resultSet.getString("photo"),
                        resultSet.getString("position"));
                    System.out.println(user.toString());
                return Optional.of(user);
            }}
        } catch (SQLException e){
            e.printStackTrace();
        }

        return Optional.empty();
    }



    public List<User> getAll() throws SQLException{

            PreparedStatement preparedStatement = sqlConnection.preparedStatement("Select * from users");
            ResultSet resultSet = preparedStatement.executeQuery();
            List<User> allUsers = new ArrayList<>();

        while (resultSet.next()) {
            String username = resultSet.getString("username");
            String gender = resultSet.getString("gender");
            String password = resultSet.getString("password");
            String email = resultSet.getString("email");
            String photo = resultSet.getString("photo");
            String position = resultSet.getString("position");


            allUsers.add(new User(username, gender, password, email, photo, position));
        }
        return allUsers;

    }
}
