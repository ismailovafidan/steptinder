package MyProject.entity;

public class User {
    private int id;
    private String fullName;
    private String gender;
    private String password;
    private String email;
    private String urlPhoto;
    private String position;

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPassword() {
        return password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User(String fullName, String gender,String password,  String email, String urlPhoto, String position  ) {
        this.fullName = fullName;
        this.gender = gender;
        this.position = position;
        this.password = password;
        this.email = email;
        this.urlPhoto = urlPhoto;
    }

    public User(int id, String fullName, String gender, String password, String email, String urlPhoto, String position) {
        this.id = id;
        this.fullName = fullName;
        this.gender = gender;
        this.password = password;
        this.email = email;
        this.urlPhoto = urlPhoto;
        this.position = position;
    }

    @Override
    public String toString() {
        return "User{" +
                "fullName='" + fullName + '\'' +
                ", gender='" + gender + '\'' +
                ", position='" + position + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", urlPhoto='" + urlPhoto + '\'' +
                '}';
    }
}
