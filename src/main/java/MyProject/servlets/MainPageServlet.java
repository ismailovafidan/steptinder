package MyProject.servlets;
import MyProject.entity.User;
import MyProject.service.MainPageService;
import MyProject.template.TemplateEngine;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class MainPageServlet extends HttpServlet {
    private final TemplateEngine engine;
    private final MainPageService mainPageService;

    public MainPageServlet(TemplateEngine engine) {
        this.engine = engine;
        mainPageService =new MainPageService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        try {
            List<User> users  = mainPageService.allUser();
            HashMap<String, Object> data = new HashMap<>();
            data.put("alluser", users);
            engine.render("people-list.ftl", data, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

}
