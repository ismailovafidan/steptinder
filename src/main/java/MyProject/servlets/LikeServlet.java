package MyProject.servlets;

import MyProject.template.TemplateEngine;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class LikeServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    public LikeServlet(TemplateEngine templateEngine){
        this.templateEngine=templateEngine;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HashMap<String,Object> data=new HashMap<>();
        templateEngine.render("like-page.ftl",data,resp);
    }
}
