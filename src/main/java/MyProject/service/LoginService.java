package MyProject.service;

import MyProject.db.UserDb;
import MyProject.entity.User;

import javax.servlet.http.Cookie;
import java.util.HashMap;
import java.util.Optional;

public class LoginService {
    private final UserDb userDb;

    public LoginService() {
        userDb = new UserDb();
    }

    public Optional<User> checkUser(String email, String password) {
        return userDb.getOneByEmail(email, password);
    }
}