package MyProject.db;

import java.util.Optional;

public interface db <A> {
    boolean add(A a);
    Optional<A> getOneByEmail(String username, String password);

}
