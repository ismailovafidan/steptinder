package MyProject.service;

import MyProject.db.UserDb;
import MyProject.entity.User;
import java.sql.SQLException;
import java.util.List;

public class MainPageService {
    private final UserDb userDb;


    public MainPageService() {
        userDb = new UserDb();
    }

    public  List<User> allUser() throws SQLException {
        return userDb.getAll();
    }

}
