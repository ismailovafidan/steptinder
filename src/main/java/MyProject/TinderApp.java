package MyProject;

import MyProject.servlets.LoginServlet;
import MyProject.servlets.MainPageServlet;
import MyProject.servlets.RegisterServlet;
import MyProject.servlets.StaticContentServlet;
import MyProject.template.TemplateEngine;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class TinderApp {
    public static void main(String[] args) throws Exception {
        Server server=new Server(8080);
        ServletContextHandler handler=new ServletContextHandler();
        TemplateEngine templateEngine=TemplateEngine.resources("/templates");

        handler.addServlet(new ServletHolder(new StaticContentServlet("src/main/resources/templates")),"/static/*");
        handler.addServlet(new ServletHolder(new RegisterServlet(templateEngine)),"/register/*");
        handler.addServlet(new ServletHolder(new LoginServlet(templateEngine)),"/login/*");
        handler.addServlet(new ServletHolder(new MainPageServlet(templateEngine)),"/MainPage/*");

        server.setHandler(handler);
        server.start();
        server.join();
    }
}
